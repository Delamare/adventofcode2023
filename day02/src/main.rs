use anyhow::{Context, Result};

fn load_input_file() -> Result<String> {
    let path = std::env::var("AOC_INPUT").context("Failed to read AOC_INPUT env var!")?;
    Ok(std::fs::read_to_string(path)?)
}

mod part1 {
    use anyhow::{bail, Context, Result};
    use std::str::FromStr;

    #[derive(Debug, Default)]
    pub(crate) struct Hand {
        pub(crate) red: usize,
        pub(crate) green: usize,
        pub(crate) blue: usize,
    }
    impl FromStr for Hand {
        type Err = anyhow::Error;
        fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
            let mut hand = Self::default();
            for cubes in s.split(',') {
                let (n, color) = cubes
                    .trim()
                    .split_once(' ')
                    .context("Failed to split in hand!")?;
                let n = n.parse().context("Failed to parse N in hand!")?;
                match color {
                    "red" => hand.red = n,
                    "green" => hand.green = n,
                    "blue" => hand.blue = n,
                    _ => bail!("Failed to match a color in hand!"),
                }
            }
            Ok(hand)
        }
    }

    pub(crate) struct Game {
        id: usize,
        pub(crate) hands: Vec<Hand>,
    }
    impl FromStr for Game {
        type Err = anyhow::Error;

        fn from_str(s: &str) -> std::result::Result<Self, Self::Err> {
            let (header, hands) = s.split_once(':').context("Game didn't have a ':'!")?;
            Ok(Self {
                id: header
                    .trim()
                    .split_once(' ')
                    .context("Failed to split header space")?
                    .1
                    .parse()
                    .context("Failed to parse game ID!")?,
                hands: hands
                    .split(';')
                    .map(|hand| hand.parse())
                    .collect::<Result<_>>()
                    .context("Failed to parse a hand")?,
            })
        }
    }

    pub(crate) fn part1(input: &[Game]) -> usize {
        let max_rgb = [12, 13, 14];
        input
            .iter()
            .map(|game| {
                for hand in &game.hands {
                    if hand.red > max_rgb[0] || hand.green > max_rgb[1] || hand.blue > max_rgb[2] {
                        return 0;
                    }
                }
                game.id
            })
            .sum()
    }
}
mod part2 {
    use crate::part1::Game;

    pub(crate) fn part2(input: &[Game]) -> usize {
        input
            .iter()
            .map(|game| {
                let mut max_r = 0;
                let mut max_g = 0;
                let mut max_b = 0;
                for hand in &game.hands {
                    max_r = max_r.max(hand.red);
                    max_g = max_g.max(hand.green);
                    max_b = max_b.max(hand.blue);
                }
                max_r * max_g * max_b
            })
            .sum()
    }
}

fn main() -> anyhow::Result<()> {
    let input: Vec<_> = load_input_file()?
        .lines()
        .map(|line| line.parse())
        .collect::<Result<_>>()
        .context("Failed to parse from input file!")?;
    dbg!(part1::part1(&input));
    dbg!(part2::part2(&input));
    Ok(())
}

#[cfg(test)]
mod test {
    use crate::{part1, part2};

    static INPUT: &str = "Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green";

    #[test]
    fn part1() {
        let input: Vec<_> = INPUT
            .lines()
            .map(|line| line.parse())
            .collect::<anyhow::Result<_>>()
            .expect("Failed to parse from input file!");
        assert_eq!(part1::part1(&input), 8);
    }

    #[test]
    fn part2() {
        let input: Vec<_> = INPUT
            .lines()
            .map(|line| line.parse())
            .collect::<anyhow::Result<_>>()
            .expect("Failed to parse from input file!");
        assert_eq!(part2::part2(&input), 2286);
    }
}
