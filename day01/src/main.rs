use anyhow::{Context, Result};

// TODO: in future, call .parse() on the string to get a generic I (input) struct
// or just call .parse() from the caller if the day needs it
fn load_input_file() -> Result<String> {
    let path = std::env::var("AOC_INPUT").context("Failed to read AOC_INPUT env var!")?;
    Ok(std::fs::read_to_string(path)?)
}
mod part1 {
    use crate::load_input_file;
    use anyhow::{Context, Result};

    pub(crate) fn parse_section(input: &str) -> Result<usize> {
        let first = input
            .chars()
            .find(|&c| c.is_ascii_digit())
            .context("No number found in input")?;
        let last = input.chars().rev().find(|&c| c.is_ascii_digit()).unwrap(); // first would have failed for this to fail
        format!("{first}{last}")
            .parse()
            .context("Failed to put parsed digit into result number type (parsed value to large for return type?)")
    }

    pub(crate) fn part1() -> usize {
        load_input_file()
            .expect("Failed to load input")
            .lines()
            .map(|line| parse_section(line).expect("Fail"))
            .sum()
    }
}
mod part2 {
    use crate::load_input_file;
    use anyhow::{Context, Result};
    use lazy_static::lazy_static;
    use regex::Regex;

    lazy_static! {
        static ref NUM_REG: Regex =
            Regex::new(r"(?m)\d|(?P<word>one|two|three|four|five|six|seven|eight|nine)")
                .expect("Bad NUM_REG!");
    }

    const NUM_WORDS: [&str; 9] = [
        "one", "two", "three", "four", "five", "six", "seven", "eight", "nine",
    ];

    fn decode_capture(captures: &regex::Captures) -> Result<usize> {
        let ret = if let Some(c) = captures.name("word") {
            NUM_WORDS
                .iter()
                .enumerate()
                .find(|(_, &x)| x == c.as_str())
                .context("NUM_WORDS and NUM_REG got out of sync!")?
                .0
                + 1
        } else {
            captures[0]
                .parse()
                .context("Somehow failed to parse a digit")?
        };
        Ok(ret)
    }

    pub(crate) fn parse_section(input: &str) -> Result<usize> {
        let first_cap = &NUM_REG
            .captures_iter(input)
            .next()
            .context("Failed to find a number!")?;
        let last_cap = &NUM_REG
            .captures_iter(input)
            .last()
            .context("Failed to find a number!")?;
        let first = decode_capture(first_cap)?;
        let last = decode_capture(last_cap)?;
        Ok((first * 10) + last)
    }

    pub(crate) fn part2() -> usize {
        load_input_file()
            .expect("Failed to load input")
            .lines()
            .map(|line| parse_section(line).expect("Fail"))
            .sum()
    }
}

fn main() {
    dbg!(part1::part1());
    dbg!(part2::part2()); // 53293 is too low but I don't feel like impl regex look arounds or redoing this :)
}

#[cfg(test)]
mod test {
    #[test]
    fn part1() {
        let input = "1abc2
pqr3stu8vwx
a1b2c3d4e5f
treb7uchet";

        let value: usize = input
            .lines()
            .map(|line| crate::part1::parse_section(line).expect("Fail"))
            .sum();
        assert_eq!(value, 142);
        let value: usize = input
            .lines()
            .map(|line| crate::part2::parse_section(line).expect("Fail"))
            .sum();
        assert_eq!(value, 142);
    }

    #[test]
    fn part2() {
        let input = "two1nine
eightwothree
abcone2threexyz
xtwone3four
4nineeightseven2
zoneight234
7pqrstsixteen";

        let value: usize = input
            .lines()
            .map(|line| crate::part2::parse_section(line).expect("Fail"))
            .sum();
        assert_eq!(value, 281);
    }
}
